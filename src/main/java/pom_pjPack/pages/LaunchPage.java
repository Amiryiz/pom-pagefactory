package pom_pjPack.pages;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pom_pjPack.base.BasePage;
import pom_pjPack.util.Constants;

public class LaunchPage extends BasePage {


    // WebElements of Launch page

    @FindBy(className="zh-customers")
    public WebElement Customers;

    @FindBy(className="zh-support")
    public WebElement Support;

    @FindBy(className="zh-login")
    public WebElement Login;

    @FindBy(className="zh-signup")
    public WebElement SignUp;

    //Constructor
    public LaunchPage(WebDriver driver, ExtentTest eTest) {

        this.driver = driver;
        this.eTest = eTest;

    }

    //Reusable methods of Launch page

    public boolean goToLoginPage() {
        //Selenium Automation code for taking the user to the login page

        driver.get(Constants.APP_URL);

        eTest.log(LogStatus.INFO,"Application URL" + Constants.APP_URL+ "got opened");

        Login.click();

        eTest.log(LogStatus.INFO,"Login option got clicked");

        LoginPage loginPage = new LoginPage(driver,eTest);

        PageFactory.initElements(driver,loginPage);

        boolean statusLogin = loginPage.doLogin();

        return statusLogin;

    }



}
