package pom_pjPack.pages;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pom_pjPack.base.BasePage;


public class HomePage extends BasePage {


    @FindBy(xpath="//span[@class='zicon-apps-crm zicon-apps-128']")
    public WebElement CRM;

    @FindBy(xpath="//div[@id='zl-category-1']//span[@class='zicon-apps-48 zicon-apps-mail']")
    public WebElement Mail;

    @FindBy(xpath="//div[@id='zl-category-1']//span[@class='zicon-apps-48 zicon-apps-projects']")
    public WebElement Projects;

    @FindBy(xpath="//div[@id='zl-category-1']//span[@class='zicon-apps-48 zicon-apps-creator']")
    public WebElement Creator;

     @FindBy(xpath="//div[@id='zl-category-1']//span[@class='zicon-apps-48 zicon-apps-books']")
    public WebElement Books;

     @FindBy(xpath="//div[@id='zl-category-1']//span[@class='zicon-apps-48 zicon-apps-support']")
    public WebElement Desk;

     //Constructor

     public HomePage(WebDriver driver, ExtentTest eTest){

         this.driver=driver;

         this.eTest=eTest;
     }

     public boolean verifyDisplayHomePage(){

         return isElementPresent(CRM);
     }

}
