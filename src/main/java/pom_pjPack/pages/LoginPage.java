package pom_pjPack.pages;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pom_pjPack.base.BasePage;
import pom_pjPack.util.Constants;

public class LoginPage extends BasePage {


    // WebElements of Launch page

    @FindBy(id="lid")
    public WebElement EmailInput;

    @FindBy(id="pwd")
    public WebElement PasswordInput;

    @FindBy(id="signin_submit")
    public WebElement SignInSubmit;

    //Constructor

    public LoginPage(WebDriver driver, ExtentTest eTest) {

        this.driver = driver;

        this.eTest =eTest;

    }

    public boolean doLogin() {

        //Selenium Automation code for logging into the application
        EmailInput.sendKeys(Constants.USERNAME);

        eTest.log(LogStatus.INFO,"Username got entered into the Email address field");

        PasswordInput.sendKeys(Constants.PASSWORD);

        eTest.log(LogStatus.INFO,"Password got entered into the password field");

        SignInSubmit.click();

        eTest.log(LogStatus.INFO,"SignIn button got clicked");

        HomePage homePage =new HomePage(driver,eTest);

        PageFactory.initElements(driver,homePage);

        boolean loginStatus = homePage.verifyDisplayHomePage();

        return loginStatus;
    }



}
