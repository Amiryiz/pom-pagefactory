package pom_pjPack.test.base;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.Assert;
import pom_pjPack.util.Constants;
import pom_pjPack.util.ExtentManager;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    // Global variables
    public ExtentReports eReport = ExtentManager.getInstance();
    public ExtentTest eTest = eReport.startTest("LoginTest");

    public WebDriver driver = null;

    public void openBrowser(String browserType) {

        if (browserType.equalsIgnoreCase("firefox")){

            System.setProperty("webdriver.gecko.driver", Constants.FF_DRIVER);

            driver = new FirefoxDriver();
        }

        else if (browserType.equalsIgnoreCase("chrome")){

                    System.setProperty("webdriver.chrome.driver", Constants.CHROME_DRIVER);

                    driver = new ChromeDriver();
        }

        else if (browserType.equalsIgnoreCase("opera")){

                    System.setProperty("webdriver.opera.driver", Constants.OPERA_DRIVER);

                    driver = new OperaDriver();
        }

        eTest.log(LogStatus.INFO,"Successfully opened the Browser" + browserType);

        driver.manage().window().maximize();

        eTest.log(LogStatus.INFO,"Browser got maximized");

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    public void reportPass(String message){

        eTest.log(LogStatus.PASS,message);
    }

    public void reportFail(String message) {

        eTest.log(LogStatus.FAIL,message);

        //Take screenshot

        //fail T.C on testNG
        Assert.fail(message);
    }

}
