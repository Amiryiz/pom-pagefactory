package pom_pjPack.test;

import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pom_pjPack.pages.LaunchPage;
import pom_pjPack.test.base.BaseTest;
import pom_pjPack.util.Constants;

public class LoginTest extends BaseTest {

    @Test
    public void testLogin() {

        eTest.log(LogStatus.INFO,"Login Test has started");

        openBrowser(Constants.BROWSER_NAME);

        LaunchPage launchPage = new LaunchPage(driver,eTest);

        // init the elements in the LaunchPage
        PageFactory.initElements(driver,launchPage);

        boolean statusLogin = launchPage.goToLoginPage();

        if (statusLogin) {

            //Pass the T.C
            reportPass("Login Test passed");
        }

        else {
            //Fail the T.C
            reportFail("Login Test failed");

        }

    }

    @AfterMethod
    public void testClosure() {

        //generate the report file
       if(eReport!=null) {
           eReport.endTest(eTest);
           eReport.flush();
       }

       if(driver!=null)
           driver.quit();
    }

}
